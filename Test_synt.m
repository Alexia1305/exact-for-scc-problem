clear variables; clc;




%% chose  test 
% test time exact 
test1=0 ;

%% Test1: Tests sur le temps de r�solution 
if test1==1
    %% Data generation
    % Select a subset of the USGS dataset to build a dictionary D
    load("data/USGS_Library.mat");
    [m, nbcol_usgs] = size(datalib);
     %% Initialisation
    itt=5;
    s=10; % start s
    n=20; % start n 
    r=4;
    nl=0.1; % pourcentage de bruit 
    spas=10;
    npas=20;
    ntest=10;
    tl=60*60; % limit time (infini) 
    tempsE=zeros(ntest,1); 
    ls=linspace(s,s*ntest,ntest);
    M=1;

    illcondA=0; % bad conditions 
    for i=1:ntest
        disp(s);
        for j=1:itt
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
         D = datalib(:,randperm(nbcol_usgs-5, s)+5);
       
            if illcondA == 1
            [u,~,v] = svd(D);
         d = diag( logspace(-6,0,s) );
          D = u(:,1:s)*d(1:s,1:s)*v(:,1:s)';
            end
          % normalisation des colonnes de D 
         for j=1:s
            D(:,j)=D(:,j)./norm(D(:,j));
         end 
        % Generate synthetic data
        realH = zeros(s, n);
         realJ = randperm(s, r);
         realH(realJ,:) = randn(r, n);
        X = D*realH;
         % Generate synthetic data
      realH = zeros(s, n);
      realJ = randperm(s, r);
       realH(realJ,:) = rand(r, n);
      X = D*realH;

        % Add noise
         noise = randn(m,n);  
      Xn = X + nl(1) * noise/norm(noise,'fro') * norm(X,'fro');

     %% Run algorithms

   
            tic;
          [J,H,results] = exact(Xn, D, r,tl,0,M);
         tE=toc;
       fprintf('Le temps d execution methode exacte %d \n',tE);

    %% Plots and comparison
   
     
           tempsE(i)=tempsE(i)+tE; 
  	 
   
        end 
    
    
   
        tempsE(i)=tempsE(i)/itt;
 
        s=s+spas;
     n=s*2;
    
    end 
  %% Plots and comparison

    % Evolution du nombre de succ�s en fct du niveau de bruit
    figure
    plot(ls,tempsE)
    title('Evolution du temps en fonction de s')
    xlabel('Nombre de colonnes du dictionnaire s')
    ylabel('Temps (secondes)') 
end 

%% Test2 : Test heuristic and hybrid heuristic-exact on synthetic data 

%% Parameters

M=100;
s = 100;   % number of columns of the dictionary D, to extract from USGS
rmax=30; % number of columns extract by heuristic for hybrid 
r = 5;    % number of columns of W, to select from the dictionary
tl =60*5; % limit time for the exact solving  
n = 100;  % number of data points (numer of columns of input X)
option=0; % NN constraint 
itt=10;% itt�ration by noise level  

nb=10; % number of noise level between 10^-10 et 1

nl = logspace(-3,-0.5,nb); % noise level to add to generated data

%% Data generation
% Select a subset of the USGS dataset to build a dictionary D
load("data/USGS_Library.mat");
[m, nbcol_usgs] = size(datalib);


 

    
    %% Initialisation 
    rS=zeros(nb,1); % greedy
    rE=zeros(nb,1); % exacte 
    rE2=zeros(nb,1); % exacte 
    rO=zeros(nb,1);
    tS=zeros(nb,1);
    tE=zeros(nb,1);
    tE2=zeros(nb,1);
    tO=zeros(nb,1);
    for i=1:nb
     for j=1:itt
        disp(i);
        % Build small dictionary by taking s random column from USGS and avoid the
         % first 5 elements (werid spectra)
       D = datalib(:,randperm(nbcol_usgs-5, s)+5);
       
        % normalisation des colonnes de D 
        for j=1:s
            D(:,j)=D(:,j)./norm(D(:,j));
        end 
     % Generate synthetic data
     realH = zeros(s, n);
     realJ = randperm(s, r);
     realH(realJ,:) = randn(r, n);
     X = D*realH;

    % Add noise
     noise = randn(m,n);  
     Xn = X + nl(i) * noise/norm(noise,'fro') * norm(X,'fro');

    %% Run algorithms
    % NNS-SP algorithm
    tic;
    [X_nnssp,J_nnssp]=NNS_greedy(D,Xn,r,'alg','sp','nonneg','off','simult','on');
    tS(i)=tS(i)+toc;
     rS(i)=rS(i)+(10-length(setdiff(J_nnssp,realJ)))/10;
     % NNS-SP algorithm + exact 
     tic;
    [X_nnssp,Jnn]=NNS_greedy(D,Xn,rmax(1),'alg','sp','nonneg','off','simult','on');
     [J,H,results] = exact(Xn, D(:,Jnn), r,tl,option,M);
     tE(i)=tE(i)+toc;
     J1=Jnn(J);
      rE(i)=rE(i)+(10-length(setdiff(J1,realJ)))/10;
      
    % NNS-OMP algorithm
    tic
    [X_nnssp,J_nnssp]=NNS_greedy(D,Xn,r,'alg','omp','nonneg','off','simult','on');
    tO(i)=tO(i)+toc;
     rO(i)=rO(i)+(10-length(setdiff(J_nnssp,realJ)))/10;
     % NNS-OMP algorithm + exact 
     tic;
    [X_nnssp,Jnn]=NNS_greedy(D,Xn,rmax(1),'alg','omp','nonneg','off','simult','on');
     [J,H,results] = exact(Xn, D(:,Jnn), r,tl,option,M);
     tE2(i)=tE2(i)+toc;
     J1=Jnn(J);
      rE2(i)=rE2(i)+(10-length(setdiff(J1,realJ)))/10;
   
        end 
    rE(i)=rE(i)/itt;
    rS(i)=rS(i)/itt;
    rO(i)=rO(i)/itt; 
    rE2(i)=rE2(i)/itt;
    tE(i)=tE(i)/itt;
    tS(i)=tS(i)/itt;


    tO(i)=tO(i)/itt; 
    tE2(i)=tE2(i)/itt;
    end 
  %% Plots and comparison

    % % Evolution de l'erreur en fct du niveau de bruit
   figure
    plot(db(nl),rS.*100,db(nl),rE.*100,db(nl),rO.*100,db(nl),rE2.*100)
    legend('S-SP','S-SP+exact','S-OMP','S-OMP+exact')
    xlabel('Noise level (dB)')
    ylabel('Percentage of correct columns (%)')
    ytickformat('percentage')
    
    figure
    plot(db(nl),tS,db(nl),tE,db(nl),tO,db(nl),tE2)
    legend('S-SP','S-SP+exact','S-OMP','S-OMP+exact')
    xlabel('Noise level (dB)')
    ylabel('Time')
   


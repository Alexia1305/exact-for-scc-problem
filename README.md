# Simultaneous Sparse Coding
Simultaneous Sparse Coding consists in the following problem:
given $`X \in \mathbb{R}^{m \times n}`$, $`D \in \mathbb{R}^{m \times s}`$, and  $`r \in \mathbb{N}`$,
find $`H \in \mathbb{R}^{s \times n}`$ by solving
$`\min_{H} \|X-DH\|_F^2 \text{ tel que } \|H\|_{row-0} \leq r `$.

## Test

Test_real.m Test on HSI

Test_synt.m Test on synthetic data 

## Algo 

exact.m algorithm exact for SSC

exactM.m algorithm exact for SSC whith Mij different for each Hij ( constraint)






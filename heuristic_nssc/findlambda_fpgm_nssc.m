function [H,lambda] = findlambda_fpgm_nssc(X,D,r,options)
%FINDLAMBDA_FPGM_NSSC Given an int r find one right lambda such that
% running nnls_FPGM_nssc with this lambda produces H with r non-zero rows.
% Performs a simple binary search.
lambda = 1;
lambda_lower = 0;
lambda_upper = 0;
cur_r = 0;
fprintf("lambda\tcur_r\n")
while cur_r ~= r
    [H,~,~] = nnls_FPGM_nssc(X, D, lambda, options);
    cur_r = length(nonzerorows(H));
    fprintf("%d\t%d\n", lambda, cur_r);
    if cur_r < r
        lambda_upper = lambda;
        lambda = (lambda + lambda_lower) / 2;
    elseif cur_r > r
        lambda_lower = lambda;
        if lambda_upper ~= 0
            lambda = (lambda + lambda_upper) / 2;
        else
            lambda = lambda * 2;
        end
    end
end
end


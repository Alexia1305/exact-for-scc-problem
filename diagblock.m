% Construction of a sparse n-by-n block matrix with Q's on the diagonal
% blocks, where Q is a square matrix

function bigQ = diagblock(Q,n) 

if n == 1 
    bigQ = sparse(Q); 
else
    q = length(Q); 
    if mod(n,2) == 0 % n is even
        halfbigQ = diagblock(Q,n/2); 
        hq = length(halfbigQ); 
        bigQ = [      halfbigQ        sparse(hq,hq); ... 
                    sparse(hq,hq)     halfbigQ ];
    else % n is odd
        halfbigQ = diagblock(Q,(n-1)/2); 
        q = length(Q); 
        hq = length(halfbigQ); 
        bigQ = [     sparse(Q)      sparse(q,hq)    sparse(q,hq); ... 
                     sparse(hq,q)   halfbigQ        sparse(hq,hq); ... 
                     sparse(hq,q)   sparse(hq,hq)     halfbigQ ];
    end
end
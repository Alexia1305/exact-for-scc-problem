function [J,H,results] = exactM(X, D, r,tl)
%MYALGO Algorithm to solve Nonnegative Simultaneous Sparse Coding
%   Given X, D, r, and lambda find H by solving the following problem:
%   min_H ||X-DH||_2^2 s.t. H>=0 and H has at most r non-zero rows

% TODO Alexandra 
% Replace this random matrices by actual MIP-based solution
% Feel free to change the structure :)

% Résolution avec la matrice H remplacé par un long vecteur de variables
% avec des variables binaires z 

[~, n] = size(X);
[m, s] = size(D);
H=zeros(s,n);
J=zeros(1,r);

% Choix de M 
M=D'*X;
for i=1:s
    M(i,:)=M(i,:)./(D(:,i)'*D(:,i));
end 

% matrice objectif 
Q=sparse(s*n+s,s*n+s);

DtD=sparse(D'*D);
XD=-2*X'*D;
%disp('1');
qt=reshape(XD',[],1);
qt=qt';
%disp('2');
qt=[qt,zeros(1,s)];
%disp('3');
Q(1:n*s,1:n*s)=diagblock(DtD,n);
% QCell = repmat({DtD}, 1, n);
% disp('4');
% Q(1:n*s,1:n*s) = blkdiag(QCell{:});
% for j=0:n-1
%     if(mod(j,1000)==0)
%         disp(j);
%     end 
%     Q(1+j*s:s+j*s,1+j*s:s+j*s)=DtD;
% end 
  
%disp('obj done');
model.Q = Q;
model.obj = full(qt');
%disp('obj done');
%variables 
model.vtype(1:n*s)='C'; %H
model.vtype(n*s+1:n*s+s)='B';%y


%initialisation de matrice contrainte 
A=sparse(2*s*n+1,n*s+s);
l=zeros(2*s*n+1,1);
% contrainte 1 
% A(1:n*s,n*s+1:n*s+s)=repmat(spdiags(-M*ones(s,1),0,s,s),n,1);
A(1:n*s,1:n*s)=spdiags(ones(n*s,1),0,n*s,n*s);
for j=0:n-1
    for i=1:s
        A(i+j*s,n*s+i)=-M(i,j+1);
    end 
end 
 %contrainte 2
 A(n*s+1,n*s+1:n*s+s)=ones(1,s);
 l(n*s+1)=r;
%contrainte 3 
A(n*s+2:2*n*s+1,1:n*s)=spdiags(ones(n*s,1),0,n*s,n*s);
%disp('A done');
model.A = A;

model.rhs = l;

%précision sens contrainte 
model.sense(1:n*s) = '<';
model.sense(n*s+1) = '<';
model.sense(s*n+2:2*s*n+1) = '>';
% ne pas afficher en console 
params.outputflag = 1 ; 
params.timelimit=tl;
disp('model done')
gurobi_write(model, 'mqp.lp');  
results  = gurobi(model,params);

for j=0:n-1
    H(:,j+1)=results.x(1+j*s:s+j*s);
end 
J = nonzerorows(H);

% J = 1:r;
W = D(:,J);

% H = zeros(s, n);
% H(J,:) = rand(r, n);

end
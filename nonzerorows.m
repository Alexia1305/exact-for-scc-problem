function [J] = nonzerorows(X)
%ROWSPARSITY Returns the indices of non-zero rows in matrix X
nbrows = size(X, 1);
J = [];
for i = 1:nbrows
    if max(X(i,:)) > 10e-4 || max(X(i,:)) < -10e-4
        J(end+1) = i;
    end
end
end


function [X, indexSet] = NNS_greedy(Phi, Y, sparsity, varargin)

% Daeun Kim (daeunk@usc.edu) 02/01/2016
%
% 
% This code provides a MATLAB software implementation for the algorithms we
% proposed and evaluated in:
% 
% [1] D. Kim, J. P. Haldar.  
%     "Greedy Algorithms for Nonnegativity-Constrained Simultaneous Sparse 
%          Recovery." 
%     Signal Processing 2016, In Press.
%     http://dx.doi.org/10.1016/j.sigpro.2016.01.021
%
%
% The code assumes a Multiple Measurement Vector (MMV) formulation, where 
% the measured data is modeled as:
%
%                  Y = Phi*X + N
%
% Here, Phi is the dictionary matrix (size: m x n), Y is the data matrix
% (size: m x k), X is the unknown sparse signal matrix (k x n) to be
% estimated, and N is an unknown additive noise matrix (size m x k).
%
% This code estimates X using greedy algorithms, and includes four
% different varieties of greedy algorithms:
%      *Orthogonal matching pursuit (OMP)
%      *Subspace pursuit (SP)
%      *Compressive sampling matching pursuit (CoSaMP)
%      *Hard thresholding pursuit (HTP) 
% 
% These algorithms can be used in combination with nonnegativity (NN)
% constraints, simultaneous sparsity (S) constraints, or both (NNS).
%
%
% The function is called using:
% [X,indexSet] = NNS_greedy(Phi,Y,sparsity,'PARAM1',val1,'PARAM2',val2,...)
% 
% 
% Inputs: 
%     Phi: Dictionary matrix (size: m x n)
%     Y: Data/measurement matrix (size: m x k)
%     sparsity: A number of nonzero elements in a column vector of X
%
%     Additional input arguments can also be supplied:
%
%      Parameter       Description: Value
%----------------------------------------------------------------------
%      'algorithm'     select an algorithm: 
%                      'omp'(default), 'sp', 'cosamp', 'htp'
%      'nonnegative'   turn on the nonnegative constraint:
%                      'on' (default)', 'off'
%      'simultaneous'  turn on the simultaneous sparsity constraint:
%                      'on' (default)', 'off'
%      'maxIter'       SP, CoSaMP and HTP algorithms allow selection of 
%                      the maximum number of iterations. 100 is default 
%                      value.
%
%
% Ouputs:
%     X: Recovered signal (size: n x k) 
%     indexSet: Support set. 
%
% Example: Recover signals using the NNS-SP algorithm  
% [X,indexSet] = ...
%       NNS_greedy(Phi,Y,sparsity,'alg','sp','nonneg','on', 'simult','on');
%
%
% 
% This software is available from http://mr.usc.edu/download/nns-greedy/.  
% As described on that page, use of this software (or its derivatives) in 
% your own work requires that you cite the paper that describes these 
% algorithms [1].


%% Check required arguments
narginchk(3,Inf);
[m1, n] = size(Phi);
[m2, k] = size(Y);

if(isempty(Phi) || isempty(Y)) 
    error('Input Error: The dictionary or the data is empty.')
end

if(m1~=m2)
    error('Input Error: The row numbers of the dictionary and the data should be same.')
else
    m = m1;
end

if(isempty(sparsity) || (sparsity <= 0) || (sparsity > n))
    error('Input Error: "sparsity" is not a valid number.')
end

%% Process optional process parameter name/value pairs 
pnames = {'algorithm' 'nonnegative' 'simultaneous' 'maxIter'};
dflts  = {      'omp'          'on'           'on'      100 };

[alg, nonneg, simult, maxIter] = ... 
    internal.stats.parseArgs(pnames,dflts,varargin{:});
if(isempty(maxIter) || maxIter <= 0)
    error('Input Error: "maxIter" is not a valid number.');
end

alg = ...
internal.stats.getParamVal(alg, {'omp','sp','cosamp','htp'}, 'algorithm');

nonneg = internal.stats.getParamVal(nonneg,{'on' 'off'},'nonnegative');
is_NN = strncmp('on', nonneg, numel(nonneg));

simult = internal.stats.getParamVal(simult,{'on' 'off'},'simultaneous');
is_S = strncmp('on', simult, numel(simult));


%% Initialization
X = zeros(n,k);
indexSet = [];

%% Conditioning the matrix / Centralization of the matrix columns
C = eye(size(Phi,1)) - (1/m)*ones(size(Phi,1));
CPhi = C*Phi;
CY = C*Y; 

%%
%fprintf('Reconstructing... \n')
%fprintf('Using ')

switch alg
    case 'omp'
        [X, indexSet] = omp(CPhi, CY, sparsity, is_NN, is_S);
    case 'sp'
        [X, indexSet] = sp(CPhi, CY, sparsity, maxIter, is_NN, is_S);
    case 'cosamp'
        [X, indexSet] = cosamp(CPhi, CY, sparsity, maxIter, is_NN, is_S);
    case 'htp'
        [X, indexSet] = htp(CPhi, CY, sparsity, maxIter, is_NN, is_S);
end
        
%fprintf('Done! \n \n')
return;

%% ========================================================================
function [X, indexSet] = omp(A, B, sparsityLevel, isNonneg, isSimult)
    % ininalization
    [m,n] = size(A);
    [m,k] = size(B);

    X = zeros(n,k);
    R = B; % residual matrix

    indexSet = [];
    notIndexSet = setdiff([1:n], indexSet);

    % compute column norms of A
    colNormA = zeros(n,1);
    for i = 1:n
        colNormA(i) = norm(A(:,i),2);
    end
%------------------------------------------------------------------------
    if(isNonneg && isSimult)
        %fprintf('NNS-OMP algorithm... \n')
        %loop
        tic
        for itr = 1:sparsityLevel
            X = zeros(n,k);
    
           coh = (A(:,notIndexSet)'*R);
           coh(find(coh <0 )) = 0;   
           [junk, index] =   max(sum(coh,2)./colNormA(notIndexSet));

           indexSet = union(indexSet,notIndexSet(index));
            notIndexSet = notIndexSet([1:index-1,index+1:end]);

           for  j =1:k
               X(indexSet,j) = lsqnonneg(A(:,indexSet),B(:,j));
           end
           R = B - A(:,indexSet)*X(indexSet,:);
        end
        T = toc;
%------------------------------------------------------------------------        
    elseif(~isNonneg && isSimult)
        %fprintf('S-OMP algorithm... \n')
        %loop
        tic
        for itr = 1:sparsityLevel
          X = zeros(n,k);
          [junk, index] =   max(sum(abs(A(:,notIndexSet)'*R),2)./colNormA(notIndexSet));
           
          indexSet = union(indexSet,notIndexSet(index));
          notIndexSet = notIndexSet([1:index-1,index+1:end]);

          X(indexSet,:) = inv(A(:,indexSet)'*A(:,indexSet))*A(:,indexSet)'*B;

          R = B - A(:,indexSet)*X(indexSet,:);
        end
        T = toc;
%------------------------------------------------------------------------       
    elseif(isNonneg && ~isSimult)        
        %fprintf('NN-OMP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);
        for j = 1:k
            x_j = zeros(n,1);
            r_j = R(:,j);
            b_j = B(:,j);
            indexSet_j = [];
            notIndexSet = setdiff([1:n], indexSet_j);
            %loop
            for itr = 1:sparsityLevel
                coh = A(:,notIndexSet)'*r_j; 
                coh(find(coh <0 )) = 0;
                
                [val, index] =   max(coh./colNormA(notIndexSet));
    
                indexSet_j = union(indexSet_j,notIndexSet(index));
                notIndexSet = notIndexSet([1:index-1,index+1:end]);

                x_j(indexSet_j) = lsqnonneg(A(:,indexSet_j),b_j);
                r_j = b_j - A(:,indexSet_j)*x_j(indexSet_j);
            end
            X(:,j) = x_j;
            indexSet(:,j) = indexSet_j;
        end
        T = toc;
%------------------------------------------------------------------------        
    else
        %fprintf('OMP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);        
        for j = 1:k
            x_j = zeros(n,1);
            r_j = R(:,j);
            b_j = B(:,j);
            indexSet_j = [];
            notIndexSet = setdiff([1:n], indexSet_j);
            %loop 
            for itr = 1:sparsityLevel
                [junk,index] = max(abs(A(:,notIndexSet)'*r_j)./colNormA(notIndexSet));
    
                indexSet_j = union(indexSet_j,notIndexSet(index));
                notIndexSet = notIndexSet([1:index-1,index+1:end]);

                x_j(indexSet_j) = inv(A(:,indexSet_j)'*A(:,indexSet_j))*A(:,indexSet_j)'*b_j;
                r_j = b_j - A(:,indexSet_j)*x_j(indexSet_j);
            end
            X(:,j) = x_j;
            indexSet(:,j) = indexSet_j;
        end
        T = toc;
    end
    %disp(['Reconstruction time: ', num2str(T), ' sec.'])
return;

%% ========================================================================
function [X, indexSet] = sp(A, B, sparsityLevel, maxIter, isNonneg, isSimult)
    % ininalization
    [m,n] = size(A);
    [m,k] = size(B);

    X = zeros(n,k);
    S = sparsityLevel;
    
    % compute column norms of A
    colNormA = zeros(n,1);
    for i = 1:n
        colNormA(i) = norm(A(:,i),2);
    end
 %------------------------------------------------------------------------
    if(isNonneg && isSimult)
        %fprintf('NNS-SP algorithm... \n')
        %loop
        tic
        coh = A'*B;
        coh(find(coh<0)) = 0;
        coh = sum(coh,2)./colNormA;
       [~, index] = sort(coh,'descend');
        T0 = index(1:S);

        X0 = zeros(S,k);
        for j =1:k
            X0(:,j) = lsqnonneg(A(:,T0),B(:,j));
        end
        R = B - A(:,T0)*X0;

        for i = 1:maxIter    
            coh = A'*R;
            coh(find(coh<0)) = 0;
            coh = sum(coh,2)./colNormA;
            [~, index] = sort(coh,'descend');

            T1 = index(1:S);
            T1 = union(T0, T1);

            X1 = zeros(numel(T1),k);
            for j = 1:k
                X1(:,j) = lsqnonneg(A(:,T1),B(:,j));
            end
            
            X1 = sqrt(sum(X1.^2,2)); %l2-norm of each row    
            [~, x_index] = sort(X1, 'descend');            
            T2  = T1(x_index(1:S));
    
            X2 = zeros(numel(T2),k);
            for j=1:k
                X2(:,j) = lsqnonneg(A(:,T2),B(:,j));
            end
    
            R2  = B - A(:,T2)*X2; 
    
            if (norm(R2,2) >= norm(R,2))
                break
            else
                R = R2;
                T0 = T2;
                X0 = X2;
            end    
        end

        indexSet = T0;
        X(T0,:) = X0;
               
        T = toc;
 %------------------------------------------------------------------------       
    elseif(~isNonneg && isSimult)
        %fprintf('S-SP algorithm... \n')
        %loop
        tic
        coh = sum(abs(A'*B),2)./colNormA;
        [~, index] = sort(coh,'descend');
        T0 = index(1:S);

        X0 =  inv(A(:,T0)'*A(:,T0))*A(:,T0)'*B;
        R = B - A(:,T0)*X0;

        for i = 1:maxIter
    
            coh = sum(abs(A'*R),2)./colNormA;
            [~, index] = sort(coh,'descend');

            T1 = index(1:S);
            T1 = union(T0, T1);
    
            X1 = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*B;
            X1 = sqrt(sum(X1.^2,2)); %l2-norm of each row
    
            [~, x_index] = sort(X1, 'descend');
            
            T2 = T1(x_index(1:S));
            X2 = inv(A(:,T2)'*A(:,T2))*A(:,T2)'*B;
            R2  = B - A(:,T2)*X2; 
    
            if (norm(R2,2) >= norm(R,2))
                break
            else
                R = R2;
                T0 = T2;
                X0 = X2;
            end
    
        end
        
        indexSet = T0;
        X(T0,:) = X0;
               
        T = toc;
 %------------------------------------------------------------------------             
    elseif(isNonneg && ~isSimult)        
        %fprintf('NN-SP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);
        for j = 1:k
            b_j = B(:,j);
            
            coh = A'*b_j./colNormA;
            coh(find(coh<0)) = 0 ;
            [~, index] = sort(coh,'descend');

            T0 = index(1:S);
            x0 = lsqnonneg(A(:,T0),b_j);

            r0 = b_j - A(:,T0)*x0; % residual

            %loop
            for itr = 1:maxIter
                coh = A'*r0./colNormA;
                coh(find(coh<0)) = 0;    
                [~, index] = sort(coh,'descend');

                T1 = index(1:S);
                T1 = union(T0, T1);
                
                x1 = lsqnonneg(A(:,T1),b_j);
                [~, x_index] = sort(x1, 'descend');
    
                T2 = T1(x_index(1:S));    
                x2 = lsqnonneg(A(:,T2),b_j);  
                r2 = b_j - A(:,T2)*x2; 
        
                if (norm(r2,2) >= norm(r0,2))
                    break
                else
                    r0 = r2;
                    T0 = T2;
                    x0 = x2;
                end            
            end
            
            X(T0,j) = x0;
            indexSet(:,j) = T0;
        end
        T = toc;
        
  %------------------------------------------------------------------------                   
    else
        %fprintf('SP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);        
        for j = 1:k
            b_j = B(:,j);
                        
            coh = abs(A'*b_j)./colNormA;
            [~, index] = sort(coh,'descend');
            
            T0 = index(1:S);
            x0 = inv(A(:,T0)'*A(:,T0))*A(:,T0)'*b_j;

            r0 = b_j - A(:,T0)*x0;

            %loop 
            for itr = 1:maxIter
                coh = abs(A'*r0)./colNormA;
                [~, index] = sort(coh,'descend');
                T1 = index(1:S);
                T1 = union(T0, T1);

                x1 = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*b_j;
                [~, x_index] = sort(abs(x1), 'descend');
    
                T2 = T1(x_index(1:S));    
                x2 =  inv(A(:,T2)'*A(:,T2))*A(:,T2)'*b_j;  
                r2 = b_j - A(:,T2)*x2; 
                                
                if (norm(r2,2) >= norm(r0,2))
                    break
                else
                    r0 = r2;
                    T0 = T2;
                    x0 = x2;
                end            
            end
            
            X(T0,j) = x0;
            indexSet(:,j) = T0;
        end
        T = toc;
    end
    
    %disp(['Reconstruction time: ', num2str(T), ' sec.'])
return;

%% ========================================================================
function [X, indexSet] = cosamp(A, B, sparsityLevel, maxIter, isNonneg, isSimult)
    % ininalization
    [m,n] = size(A);
    [m,k] = size(B);

    X = zeros(n,k);
    R = B; % residual matrix
    S = sparsityLevel;
    
    % compute column norms of A
    colNormA = zeros(n,1);
    for i = 1:n
        colNormA(i) = norm(A(:,i),2);
    end
 %------------------------------------------------------------------------
    if(isNonneg && isSimult)
        %fprintf('NNS-CoSaMP algorithm... \n')
        %loop
        tic
        coh = A'*B;
        coh(find(coh<0)) = 0;
        coh = sum(coh,2)./colNormA;
        [~, index] = sort(coh,'descend');
        T0 = index(1:S);

        X0 = zeros(S,k);
        for j =1:k
            X0(:,j) = lsqnonneg(A(:,T0),B(:,j));
        end
        R = B - A(:,T0)*X0;

        for i = 1:maxIter    
            coh = A'*R;
            coh(find(coh<0)) = 0;
            coh = sum(coh,2)./colNormA;
            [~, index] = sort(coh,'descend');

            T1 = index(1:2*S);
            T1 = union(T0, T1);

            X1 = zeros(numel(T1),k);
            for j = 1:k
                X1(:,j) = lsqnonneg(A(:,T1),B(:,j));
            end
            
            l2norm_X1 = sqrt(sum(X1.^2,2)); %l2-norm of each row    
            [~, x_index] = sort(l2norm_X1, 'descend');            
            T2  = T1(x_index(1:S));
    
            X2 = X1(x_index(1:S),:);
            R2  = B - A(:,T2)*X2; 

            if (norm(R2,2) >= norm(R,2))
                break
            else
                R = R2;
                T0 = T2;
                X0 = X2;
            end    
        end

        indexSet = T0;
        X(T0,:) = X0;
               
        T = toc;
 %------------------------------------------------------------------------       
    elseif(~isNonneg && isSimult)
        %fprintf('S-CoSaMP algorithm... \n')
        %loop
        tic
        coh = sum(abs(A'*B),2)./colNormA;
        [~, index] = sort(coh,'descend');
        T0 = index(1:S);

        X0 =  inv(A(:,T0)'*A(:,T0))*A(:,T0)'*B;
        R = B - A(:,T0)*X0;

        for i = 1:maxIter
    
            coh = sum(abs(A'*R),2)./colNormA;
            [~, index] = sort(coh,'descend');

            T1 = index(1:2*S);
            T1 = union(T0, T1);
    
            X1 = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*B;
            l2norm_X1 = sqrt(sum(X1.^2,2)); %l2-norm of each row
    
            [~, x_index] = sort(l2norm_X1, 'descend');
            
            T2 = T1(x_index(1:S));
            X2 = X1(x_index(1:S),:);
            R2  = B - A(:,T2)*X2; 
    
            if (norm(R2,2) >= norm(R,2))
                break
            else
                R = R2;
                T0 = T2;
                X0 = X2;
            end
    
        end
        
        indexSet = T0;
        X(T0,:) = X0;
               
        T = toc;
 %------------------------------------------------------------------------             
    elseif(isNonneg && ~isSimult)        
        %fprintf('NN-CoSaMP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);
        for j = 1:k
            b_j = B(:,j);

            coh = A'*b_j./colNormA;
            coh(find(coh<0)) = 0 ;
            [~, index] = sort(coh,'descend');

            T0 = index(1:S);
            x0 = lsqnonneg(A(:,T0),b_j);

            r0 = b_j - A(:,T0)*x0; % residual

            %loop
            for itr = 1:maxIter
                coh = A'*r0./colNormA;
                coh(find(coh<0)) = 0;    
                [~, index] = sort(coh,'descend');

                T1 = index(1:2*S);
                T1 = union(T0, T1);
                
                x1 = lsqnonneg(A(:,T1),b_j);
                [~, x_index] = sort(x1, 'descend');
    
                T2 = T1(x_index(1:S));    
                x2 = x1(x_index(1:S));  
                r2 = b_j - A(:,T2)*x2; 
        
                if (norm(r2,2) >= norm(r0,2))
                    break
                else
                    r0 = r2;
                    T0 = T2;
                    x0 = x2;
                end            
            end
            
            X(T0,j) = x0;
            indexSet(:,j) = T0;
        end
        T = toc;
        
  %------------------------------------------------------------------------                   
    else
        %fprintf('CoSaMP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);        
        for j = 1:k
            b_j = B(:,j);
                        
            coh = abs(A'*b_j)./colNormA;
            [~, index] = sort(coh,'descend');
            
            T0 = index(1:S);
            x0 = inv(A(:,T0)'*A(:,T0))*A(:,T0)'*b_j;

            r0 = b_j - A(:,T0)*x0; % residual

            %loop 
            for itr = 1:maxIter
                coh = abs(A'*r0)./colNormA;
                [~, index] = sort(coh,'descend');
                T1 = index(1:2*S);
                T1 = union(T0, T1);

                x1 = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*b_j;
                [~, x_index] = sort(abs(x1), 'descend');
    
                T2 = T1(x_index(1:S));    
                x2 =  x1(x_index(1:S));  
                r2 = b_j - A(:,T2)*x2; 
                                
                if (norm(r2,2) >= norm(r0,2))
                    break
                else
                    r0 = r2;
                    T0 = T2;
                    x0 = x2;
                end            
            end
            
            X(T0,j) = x0;
            indexSet(:,j) = T0;
        end
        T = toc;
    end
    
    %disp(['Reconstruction time: ', num2str(T), ' sec.'])
return;

%% ========================================================================
function [X, indexSet] = htp(A, B, sparsityLevel, maxIter, isNonneg, isSimult)
    % ininalization
    [m,n] = size(A);
    [m,k] = size(B);

    X = zeros(n,k);
    S = sparsityLevel;
    
    % compute column norms of A
    colNormA = zeros(n,1);
    for i = 1:n
        colNormA(i) = norm(A(:,i),2);
    end
           
    A = A*diag(1./colNormA); % scaling

 %------------------------------------------------------------------------
    if(isNonneg && isSimult)
        %fprintf('NNS-HTP algorithm... \n')
        %loop
        tic
        
        X0 = zeros(n,k);

        X0_tmp = A'*B;
        X0_tmp(find(X0_tmp < 0)) = 0;
        X0_tmp = sqrt(sum(X0_tmp.^2,2));
        [~, index] = sort(X0_tmp, 'descend');
        T0 = index(1:S);

        R = B; % residual matrix
        %loop
        for i = 1:maxIter               
            num = A(:,T0)'*R;
            den = A(:,T0)*num;
            mu = (norm(num,'fro')^2) / (norm(den,'fro')^2);

            X0_tmp = X0 + mu*A'*R;
            X0_tmp(find(X0_tmp < 0)) = 0;
            X0_tmp = sqrt(sum(X0_tmp.^2,2));
            [~, index] = sort(X0_tmp, 'descend');
            
            T1 = index(1:S);
            X1 = zeros(n,k);
            for j = 1:k
                X1(T1,j) = lsqnonneg(A(:,T1),B(:,j));
            end
    
            R1 = B - A(:,T1)*X1(T1,:);
    

            if (norm(R1,2) >= norm(R,2))
                break
            else
                R = R1;
                T0 = T1;
                X0 = X1;
            end    
        end
        
        indexSet = T0;
        X = inv(diag(colNormA))*X0; % rescaling
               
        T = toc;
 %------------------------------------------------------------------------       
    elseif(~isNonneg && isSimult)
        %fprintf('S-HTP algorithm... \n')

        %loop
        tic
        
        X0 = zeros(n,k);

        X0_tmp = A'*B;
        X0_tmp = sqrt(sum(X0_tmp.^2,2));
        [~, index] = sort(X0_tmp, 'descend');
        T0 = index(1:S);
    
        R = B; % residual matrix
        %loop
        for i = 1:maxIter               
            num = A(:,T0)'*R;
            den = A(:,T0)*num;
            mu = (norm(num,'fro')^2) / (norm(den,'fro')^2);

            X0_tmp = X0 + mu*A'*R;
            X0_tmp = sqrt(sum(X0_tmp.^2,2));
            [~, index] = sort(X0_tmp, 'descend');
            
            T1 = index(1:S);
            X1 = zeros(n,k);
            X1(T1,:) = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*B;
            
            R1 = B - A(:,T1)*X1(T1,:);
    
            if (norm(R1,2) >= norm(R,2))
                break
            else
                R = R1;
                T0 = T1;
                X0 = X1;
            end    
        end
        
        indexSet = T0;
        X = inv(diag(colNormA))*X0; % rescaling
               
        T = toc;
 %------------------------------------------------------------------------             
    elseif(isNonneg && ~isSimult)        
        %fprintf('NN-HTP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);
        for j = 1:k
            b_j = B(:,j);
            
            x0 = zeros(n,1);
            
            x0_tmp = A'*b_j;
            x0_tmp(find(x0_tmp <0)) = 0;
            [~, index] = sort(x0_tmp, 'descend');
            T0 = index(1:S);

            r0 = b_j;
            %loop
            for itr = 1:maxIter
                num = A(:,T0)'*r0;
                den = A(:,T0)*num;
                mu = (norm(num,'fro')^2) / (norm(den,'fro')^2);

                x1_tmp = x0 + mu*A'*r0;%./colNormA;
                x1_tmp(find(x1_tmp <0)) = 0;
                [~, index] = sort(x1_tmp, 'descend');
                T1 = index(1:S);           
                
                x1 = zeros(n,1);
                x1(T1) = lsqnonneg(A(:,T1),b_j);
 
                r1 = b_j - A(:,T1)*x1(T1);
        
                if (norm(r1,2) >= norm(r0,2))
                    break
                else
                    r0 = r1;
                    T0 = T1;
                    x0 = x1;
                end            
            end
            
            X(:,j) = inv(diag(colNormA))*x0;
            indexSet(:,j) = T0;
        end
        T = toc;
        
  %------------------------------------------------------------------------                   
    else
        %fprintf('HTP algorithm... \n')
        tic
        indexSet = zeros(sparsityLevel,k);
        for j = 1:k
            b_j = B(:,j);
            
            x0 = zeros(n,1);            

            x0_tmp = A'*b_j;
            [~, index] = sort(abs(x0_tmp), 'descend');
            T0 = index(1:S);

            r0 = b_j;
            %loop
            for itr = 1:maxIter
                num = A(:,T0)'*r0;
                den = A(:,T0)*num;
                mu = (norm(num,'fro')^2) / (norm(den,'fro')^2);

                x1_tmp = x0 + mu*A'*r0;%./colNormA;
                [~, index] = sort(abs(x1_tmp), 'descend');
                T1 = index(1:S);           
                
                x1 = zeros(n,1);
                x1(T1) = inv(A(:,T1)'*A(:,T1))*A(:,T1)'*b_j;
 
                r1 = b_j - A(:,T1)*x1(T1);
        
                if (norm(r1,2) >= norm(r0,2))
                    break
                else
                    r0 = r1;
                    T0 = T1;
                    x0 = x1;
                end            
            end
            
            X(:,j) = inv(diag(colNormA))*x0;
            indexSet(:,j) = T0;
        end
        T = toc;
    end
    
    %disp(['Reconstruction time: ', num2str(T), ' sec.'])
return;
